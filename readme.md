# Route Generator

Route Generator for Laravel.

# Installation

1. Config in app.php ` Thainph\RouteGenerator\class RouteGeneratorServiceProvider extends ServiceProvider,`
2. Publish config file: `php artisan vendor:publish --tag=route-generator-config`
# How to use
Run below command to generate all routes to js:
```
php artisan route:generate {path-to-directory}
```
Using by Ziggy:
```
import route from "ziggy";
import { Ziggy } from "../plugins/your-routes";

Vue.mixin({
    methods: {
        route: (name, params, absolute) => route(name, params, absolute, Ziggy),
    }
});


```
