<?php

namespace Thainph\RouteGenerator\Console;

use Illuminate\Console\Command;
use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Reflector;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;

class GenerateRoutes extends Command
{
    protected $signature = 'route:generate {path}';

    protected $description = 'Generate routes to ziggy js';

    protected function isStartWithPrefixes($str, $prefixes = []): bool
    {
        foreach ($prefixes as $prefix) {
            if (Str::startsWith(rtrim(ltrim($str, '/'), '/'), rtrim(ltrim($prefix, '/'), '/'))) {
                return true;
            }
        }

        return false;
    }

    public function handle(): void
    {
        $targetDirectory = ltrim(rtrim($this->argument('path')));

        if (!file_exists($targetDirectory)) {
            mkdir($targetDirectory);
        }

        $domains = config('route-generator.domains');

        foreach ($domains as $name => $domain) {
            $data = [
                'url' => $domain['url'],
                'port' => null,
                'defaults' => [],
                'routes' => $this->getRoutes($domain)
            ];
            $fileName = $targetDirectory. '/' . $name . '.js';
            $this->writeToJs($fileName, json_encode($data));
        }
    }
    private function getRoutes($setting)
    {
        $data = [];
        $routes = Route::getRoutes();

        foreach ($routes as $route) {
            if (empty($setting['prefixes']) || $this->isStartWithPrefixes($route->getPrefix(), $setting['prefixes'])) {

                $data[$route->getName()] = [
                    'uri' => $this->getUri($route, $setting),
                    'methods' => $route->methods(),
                    'bindings' => $this->getBindings($route)
                ];
            }
        }

        return $data;
    }

    private function getUri($route, $setting)
    {
        $uri = $route->uri();

        foreach ($setting['replaces'] as $item) {
            $uri = Str::replace($item['search'], $item['replace'], $uri);
        }

        return rtrim(ltrim($uri, '/'), '/');
    }

    private function getBindings($route): array
    {
        $scopedBindings = method_exists(head($route) ?: '', 'bindingFields');

        $bindings = [];

        foreach ($route->signatureParameters(UrlRoutable::class) as $parameter) {
            if (! in_array($parameter->getName(), $route->parameterNames())) {
                break;
            }

            $model = class_exists(Reflector::class)
                ? Reflector::getParameterClassName($parameter)
                : $parameter->getType()->getName();
            $override = (new ReflectionClass($model))->isInstantiable()
                && (new ReflectionMethod($model, 'getRouteKeyName'))->class !== Model::class;

            // Avoid booting this model if it doesn't override the default route key name
            $bindings[$parameter->getName()] = $override ? app($model)->getRouteKeyName() : 'id';
        }

        return $scopedBindings ? array_merge($bindings, $route->bindingFields()) : $bindings;
    }

    private function writeToJs($fileName, $data)
    {
        $template = <<<JAVASCRIT
const Ziggy = $data;

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    Object.assign(Ziggy.routes, window.Ziggy.routes);
}

export {Ziggy};
JAVASCRIT;
        file_put_contents($fileName, $template);
        $this->info('All routes generated to '. $fileName.' successfully!');
    }
}
