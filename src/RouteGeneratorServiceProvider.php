<?php
namespace Thainph\RouteGenerator;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use Thainph\RouteGenerator\Console\GenerateRoutes;

class RouteGeneratorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/route-generator.php' => config_path('route-generator.php'),
        ], 'route-generator-config');


        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateRoutes::class,
            ]);
        }

    }
}
