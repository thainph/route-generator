<?php
return [
    'domains' => [
        'app' => [
            'url' => config('app.url'),
            'prefixes' => [],
            'replaces' => [],
        ],
//        'shopify' => [
//            'url' => 'https://example.com',
//            'prefixes' => [
//                'shopify/front-store/{hash}'
//            ],
//            'replaces' => [
//                [
//                    'search' => 'shopify/front-store/{hash}',
//                    'replace' => '',
//                ]
//            ]
//        ],
    ]
];
